package com.example.mywearsimulatorapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textViewResult;
    RadioButton radioButtonMale, radioButtonFemale;
    //For head
    RadioButton radioButtonHelm, radioButtonGlasses, radioButtonTeapot;
    //For torso
    RadioButton radioButtonTorso, radioButtonSuit, radioButtonPillow;
    //For legs
    RadioButton radioButtonPants, radioButtonLeggings, radioButtonFlipFlops;
    //For foots
    RadioButton radioButtonShoes, radioButtonBoots, radioButtonHorns;
    CheckBox checkBoxBrassieres;
    Button buttonWear;
    Spinner spinnerCharacter;

    boolean isRight;

    String[] characters = new String[]{"Knight", "Nobleman"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinnerCharacter = findViewById(R.id.spinnerCharacter);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,characters);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCharacter.setAdapter(spinnerAdapter);

        radioButtonMale = findViewById(R.id.radioButtonMale);
        radioButtonFemale = findViewById(R.id.radioButtonFemale);

        radioButtonHelm = findViewById(R.id.radioButtonHelm);
        radioButtonGlasses = findViewById(R.id.radioButtonGlasses);
        radioButtonTeapot = findViewById(R.id.radioButtonTeapot);

        radioButtonTorso = findViewById(R.id.radioButtonTorso);
        radioButtonSuit = findViewById(R.id.radioButtonSuit);
        radioButtonPillow = findViewById(R.id.radioButtonPillow);

        radioButtonPants = findViewById(R.id.radioButtonPants);
        radioButtonLeggings = findViewById(R.id.radioButtonLeggings);
        radioButtonFlipFlops = findViewById(R.id.radioButtonFlipFlops);

        radioButtonShoes = findViewById(R.id.radioButtonShoes);
        radioButtonBoots = findViewById(R.id.radioButtonBoots);
        radioButtonHorns = findViewById(R.id.radioButtonHorns);

        checkBoxBrassieres = findViewById(R.id.checkBoxBrassieres);

        buttonWear = findViewById(R.id.buttonWear);

        textViewResult = findViewById(R.id.textViewResult);

        radioButtonMale.setOnCheckedChangeListener(OnChecked);
        radioButtonFemale.setOnCheckedChangeListener(OnChecked);

        radioButtonHelm.setOnCheckedChangeListener(OnChecked);
        radioButtonGlasses.setOnCheckedChangeListener(OnChecked);
        radioButtonTeapot.setOnCheckedChangeListener(OnChecked);

        radioButtonTorso.setOnCheckedChangeListener(OnChecked);
        radioButtonSuit.setOnCheckedChangeListener(OnChecked);
        radioButtonPillow.setOnCheckedChangeListener(OnChecked);

        radioButtonPants.setOnCheckedChangeListener(OnChecked);
        radioButtonLeggings.setOnCheckedChangeListener(OnChecked);
        radioButtonFlipFlops.setOnCheckedChangeListener(OnChecked);

        radioButtonShoes.setOnCheckedChangeListener(OnChecked);
        radioButtonBoots.setOnCheckedChangeListener(OnChecked);
        radioButtonHorns.setOnCheckedChangeListener(OnChecked);

        checkBoxBrassieres.setOnCheckedChangeListener(OnChecked);

        isRight=false;

        Check();

        buttonWear.setOnClickListener(buttonWearOnClick);
    }

    CompoundButton.OnCheckedChangeListener OnChecked = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Check();
        }
    };

    View.OnClickListener buttonWearOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            if (radioButtonMale.isChecked())
            {
                if (isRight == true) {
                    textViewResult.setText("Правильно одет");
                } else {
                    textViewResult.setText("Неправильно одет");
                }
            }
            if (radioButtonFemale.isChecked())
            {
                if (isRight == true) {
                    textViewResult.setText("Правильно одета");
                } else {
                    textViewResult.setText("Неправильно одета");
                }
            }
        }
    };

    void Check()
    {
        int character = spinnerCharacter.getSelectedItemPosition();

        switch (character)
        {
            case 0:
                if (radioButtonMale.isChecked())
                {
                    if(radioButtonHelm.isChecked() && radioButtonTorso.isChecked() && radioButtonLeggings.isChecked() && radioButtonBoots.isChecked() && checkBoxBrassieres.isChecked())
                    {
                        isRight=false;
                    }
                    else if (radioButtonHelm.isChecked() && radioButtonTorso.isChecked() && radioButtonLeggings.isChecked() && radioButtonBoots.isChecked())
                    {
                        isRight = true;
                    }
                    else
                    {
                        isRight = false;
                    }
                }
                else if (radioButtonFemale.isChecked())
                {
                    if(radioButtonHelm.isChecked() && radioButtonTorso.isChecked() && radioButtonLeggings.isChecked() && radioButtonBoots.isChecked() && checkBoxBrassieres.isChecked())
                    {
                        isRight=true;
                    }
                    else
                    {
                        isRight = false;
                    }
                }
                break;
            case 1:
                if (radioButtonMale.isChecked())
                {
                    if(radioButtonGlasses.isChecked() && radioButtonSuit.isChecked() && radioButtonPants.isChecked() && radioButtonShoes.isChecked() && checkBoxBrassieres.isChecked())
                    {
                        isRight=false;
                    }
                    else if (radioButtonGlasses.isChecked() && radioButtonSuit.isChecked() && radioButtonPants.isChecked() && radioButtonShoes.isChecked())
                    {
                        isRight = true;
                    }
                    else
                    {
                        isRight = false;
                    }
                }
                else if (radioButtonFemale.isChecked())
                {
                    if(radioButtonGlasses.isChecked() && radioButtonSuit.isChecked() && radioButtonPants.isChecked() && radioButtonShoes.isChecked() && checkBoxBrassieres.isChecked())
                    {
                        isRight=true;
                    }
                    else
                    {
                        isRight = false;
                    }
                }
                break;
        }
    }
}
